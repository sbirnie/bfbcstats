package net.myandroidthings.bfbcs;

import java.io.ByteArrayOutputStream;
import java.util.Iterator;

import net.myandroidthings.bfbcs.model.Event;
import net.myandroidthings.bfbcs.model.Player;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.os.Handler;

public class DatabaseHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "bfbcstats.db";
	private static final int DATABASE_VERSION = 4;

	public static final String EVENTS_TABLE = "events";
	public static final String IMAGES_TABLE = "images";
	public static final String ID = "_id";
	public static final String EVENT = "event";
	public static final String COUNT = "events_remaining";
	public static final String IMG_RESOURCE = "img_resource";
	public static final String URL = "url";
	public static final String DATA = "data";
	
	private static final String ALL_EVENTS_SQL = "SELECT " + ID + ", " + EVENT + ", " + COUNT + ", " + IMG_RESOURCE + " FROM " + EVENTS_TABLE + " ORDER BY " + COUNT;
	private static final String GET_BITMAP_SQL = "SELECT " + DATA + " FROM " + IMAGES_TABLE + " WHERE " + URL + "= ?";
	private static final String INSERT_SQL = "INSERT INTO " + EVENTS_TABLE + "(" + EVENT + ", " + COUNT + ", " + IMG_RESOURCE + ") values (?, ?, ?)"; 

	
	public DatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE " + EVENTS_TABLE + " (" + 
				   ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + 
				   EVENT + " TEXT NOT NULL, " + 
				   COUNT + " INTEGER NOT NULL, " +
				   IMG_RESOURCE + " TEXT NOT NULL " +
				   ");");

		db.execSQL("CREATE TABLE " + IMAGES_TABLE + " (" +
					ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
					URL + " TEXT UNIQUE, " +
					DATA + " BLOB" +
		        	");");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS " + EVENTS_TABLE);
		db.execSQL("DROP TABLE IF EXISTS " + IMAGES_TABLE);
		onCreate(db);
	}

	public Cursor getAllEvents() {
		return getReadableDatabase().rawQuery(ALL_EVENTS_SQL, null);
	}

	public void refreshPlayer(Player player, Handler handler) throws JSONException {
		handler.sendMessage(handler.obtainMessage(20));
		getWritableDatabase().beginTransaction();
		try {
			getWritableDatabase().delete(EVENTS_TABLE, null, null);
			handler.sendMessage(handler.obtainMessage(40));
			SQLiteStatement insert = getWritableDatabase().compileStatement(INSERT_SQL);
			updateWeapons(player.getWeapons(), insert);
			handler.sendMessage(handler.obtainMessage(60));
			updateGadgets(player.getGadgets(), insert);
			handler.sendMessage(handler.obtainMessage(80));
			updateInsignias(player.getInsiginias(), insert);
			getWritableDatabase().setTransactionSuccessful();
		} finally {
			getWritableDatabase().endTransaction();
		}
	}

	public void saveImage(String url, Bitmap bitmap) {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		// Middle parameter is quality, but since PNG is lossless, it doesn't matter
		bitmap.compress(CompressFormat.PNG, 0, outputStream);
		byte[] blobData = outputStream.toByteArray();
		ContentValues values = new ContentValues();
		values.put(URL, url);
		values.put(DATA, blobData);
		getWritableDatabase().insert(IMAGES_TABLE, ID, values);
	}

	public Bitmap getImage(String url) {
		Cursor cursor = getReadableDatabase().rawQuery(GET_BITMAP_SQL, new String[] {url});
		if (cursor.moveToFirst()) {
			byte[] bitmapData = cursor.getBlob(cursor.getColumnIndex(DATA));
			return BitmapFactory.decodeByteArray(bitmapData, 0, bitmapData.length);
		} else {
			return null;
		}
	}
	
	private void updateGadgets(JSONObject gadgets, SQLiteStatement insert) throws JSONException {
		JSONObject gadget;
		String key;
		String name;
		int kills = 0;
		Event event;

		Iterator<String> keys = gadgets.keys();
		while (keys.hasNext()) {
			key = keys.next();
			gadget = gadgets.getJSONObject(key);
			name = gadget.getString("name");
			if (!gadget.has("kills")) {
				continue;
			}

			try {
				gadget.getJSONObject("stars");
			} catch(Exception e) {
				continue;
			}

			kills = gadget.getInt("kills");
			event = BfbcsApi.nextGadgetEvent(kills);
			if (event == null) {
				continue;
			}

			insert.clearBindings();
			insert.bindString(1, name);
			insert.bindLong(2, event.killsLeft);
			insert.bindString(3, event.eventName);
			insert.executeInsert();
		}
	}

	private void updateWeapons(JSONObject weapons, SQLiteStatement insert) throws JSONException {
		JSONObject weapon;
		String key;
		String name;
		int kills = 0;
		Event event;

		Iterator<String> keys = weapons.keys();
		while (keys.hasNext()) {
			key = keys.next();
			weapon = weapons.getJSONObject(key);
			name = weapon.getString("name");
			kills = weapon.getInt("kills");
			event = BfbcsApi.nextWeaponEvent(kills);
			if (event == null) {
				continue;
			}
			
			insert.clearBindings();
			insert.bindString(1, name);
			insert.bindLong(2, event.killsLeft);
			insert.bindString(3, event.eventName);
			insert.executeInsert();
		}		
	}

	private void updateInsignias(JSONArray insignias, SQLiteStatement insert) throws JSONException {
		JSONObject insignia;
		JSONObject criteria = null;
		int count;

		for (int index = 0; index < insignias.length(); index++) {
			insignia = insignias.getJSONObject(index);
			if (insignia.getInt("count") != 0) {
				// they've received this insignia
				continue;
			}
			
			try {
				criteria = insignia.getJSONObject("criteria1");
			} catch (Exception e) {
				// no criteria to test for
				continue;
			}
			count = criteria.getInt("target") - criteria.getInt("value");
			if (count < 0) {
				// in case there is a 2nd criteria and the 1st was already met.
				count = 0;
			}

			try {
				criteria = insignia.getJSONObject("criteria2");
				int count2 = criteria.getInt("target") - criteria.getInt("value");
				if (count2 > 0) {
					count = count + count2;
				}
			} catch (Exception e) {
				// no 2nd criteria
			}

			insert.clearBindings();
			insert.bindString(1, insignia.getString("name"));
			insert.bindLong(2, count);
			insert.bindString(3, insignia.getString("img"));
			insert.executeInsert();
		}
	}
}

