package net.myandroidthings.bfbcs;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Calendar;

import net.myandroidthings.bfbcs.model.Event;
import net.myandroidthings.bfbcs.model.Player;

import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

public class BfbcsApi {
	private static final int[] weapon_events = { 25, 50, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000 };
	private static final int[] gadget_events = { 10, 25, 50, 100, 150, 200, 250, 300, 350, 400, 450, 500 };

	private static final String[] event_name = { "bron", "silv", "gold", "gold", "gold", "gold", "gold", "gold", "gold", "gold", "gold", "plat" };

	private static final String STAR_PREFIX = "/misc_normal/s_";
	private static final String STAR_SUFFIX = ".png";
 
	private final static String URL = "http://api.bfbcs.com/api/";
	private final static String BFBCS_URL = "http://bfbcs.com/stats_";
	private final static String SEARCH_PARAMETER = "?search=";
	private final static String PLAYERS_PARAMETER = "?players=";
	private final static String FIELDS_PARAMETER = "&fields=weapons,gadgets,insiginias,grimg";
	private final static String FOUND_EXACT = "found_exact";
	private final static String TRUE = "true";
	private final static long REFRESH_DELAY = 1200000;	// minimum of 20 minutes.
	
	private long lastRefresh = 0; 
	private HttpClient client;
	
	public BfbcsApi(HttpClient httpClient) {
		client = httpClient;
	}

	public void postUpdateQueue(String name, String platform) throws Exception {
		URL updateUrl = new URL(BFBCS_URL + URLEncoder.encode(platform) + "/" + URLEncoder.encode(name));
		URLConnection conn = updateUrl.openConnection();
		conn.setDoOutput(true);
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write("request=addplayerqueue");
		wr.flush();

		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String line;
		while ((line = rd.readLine()) != null) {
			System.out.println(line);
		}
		wr.close();
		rd.close();
	}
	
	public Player getPlayer(String name, String platform) throws Exception {
		long now = Calendar.getInstance().getTimeInMillis();
		if ((now - REFRESH_DELAY) < lastRefresh) {
			return null;	// It's too soon to refresh
		}
		
		lastRefresh = now;
		postUpdateQueue(name, platform);

		ResponseHandler<String> responseHandler = new BasicResponseHandler();
		HttpGet getRequest = new HttpGet(URL + URLEncoder.encode(platform) + PLAYERS_PARAMETER + URLEncoder.encode(name) + FIELDS_PARAMETER);
		String getResponseBody = client.execute(getRequest, responseHandler);
		JSONObject object = (JSONObject) new JSONTokener(getResponseBody).nextValue();
		JSONArray jsonPlayers = object.getJSONArray("players");
		JSONObject player = jsonPlayers.getJSONObject(0);

		String updatedName = player.getString("name");
		String updatedPlatform = player.getString("platform");

		return new Player(updatedName, updatedPlatform, player);
	}

	public boolean isPlayerConfirmed(String name, String platform) throws Exception  {
		HttpGet request = new HttpGet(URL + URLEncoder.encode(platform) + SEARCH_PARAMETER + URLEncoder.encode(name));
		ResponseHandler<String> responseHandler = new BasicResponseHandler();
		String responseBody = client.execute(request, responseHandler);
		JSONObject object = (JSONObject) new JSONTokener(responseBody).nextValue();
		String found = object.getString(FOUND_EXACT);
		if (TRUE.equalsIgnoreCase(found)) {
			return true;
		}

		return false;
	}

	public static Event nextGadgetEvent(int kills) {
		return nextEvent(gadget_events, kills );
	}

	public static Event nextWeaponEvent(int kills) {
		return nextEvent(weapon_events, kills );
	}

	public static Event nextEvent(int[] events, int kills) {
		// Example - input kills is 83. the next event ABOVE 83 is 100. 100-83 is 17. So 17 kills until gold x2
		for (int i = 0; i < 12; i++) {
			if (events[i] > kills) {
				return new Event(events[i] - kills, STAR_PREFIX + event_name[i] + STAR_SUFFIX);
			}
		}

		return null;
	}
}
