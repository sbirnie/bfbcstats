package net.myandroidthings.bfbcs.activities;

import net.myandroidthings.bfbcs.R;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class Help extends Activity {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.help);
		TextView helpText = (TextView) findViewById(R.id.help_text);
		helpText.setText("A big thanks to bfbcs.com for supplying the API and graphics.  The first time you refresh it may take some time while images are downloaded and cached. \n\n  " + 
						"If you are getting an error bringing up your stats, go to the bfbcs.com website " +
						"and enter your player name there so they will start tracking your stats.  After that the application will be able to retrieve your information. \n\n" +
						"Feel free to post issues, questions, or suggestions to the BFBCStats Google Group - http://groups.google.com/group/bfbcstats");
	}
}
