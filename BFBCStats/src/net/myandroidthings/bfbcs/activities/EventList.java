package net.myandroidthings.bfbcs.activities;

import java.util.Calendar;

import net.myandroidthings.bfbcs.BfbcsApi;
import net.myandroidthings.bfbcs.DatabaseHelper;
import net.myandroidthings.bfbcs.ImageDownloader;
import net.myandroidthings.bfbcs.R;
import net.myandroidthings.bfbcs.model.Player;

import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class EventList extends ListActivity {
	private static final String PREF_NAME = "name";
	private static final String PREF_PLATFORM = "platform";

	private static final int HANDLER_UNCONFIRMED_PLAYER = -1;
	private static final int HANDLER_REFRESH_TOO_SOON = -2;
	private static final int HANDLER_FATAL_ERROR = -100;
	private static final int HANDLER_REFRESH_FINISHED = 100;

	private boolean playerConfirmed = false;
	private Cursor cursor;
	private ProgressBar progress;
	private EventAdapter adapter;
	private DatabaseHelper db;
	private HttpClient httpClient = new DefaultHttpClient();
	private BfbcsApi api = new BfbcsApi(httpClient);
	private Calendar last_refresh = Calendar.getInstance();

	private String name = "";
	private String prevName = "";
	private String platform = "";
	private String prevPlatform = "";

	Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case HANDLER_REFRESH_FINISHED:
				adapter.getCursor().requery();
				adapter.notifyDataSetChanged();
				last_refresh = Calendar.getInstance();
				setTitle(getResources().getString(R.string.app_name) + " - " + DateFormat.format("MM/dd/yy h:mmaa", last_refresh));
				progress.setProgress(msg.what);
				progress.setVisibility(ProgressBar.GONE);
				break;

			case HANDLER_UNCONFIRMED_PLAYER:
				// Unable to confirm player name/platform
				Toast.makeText(getApplicationContext(), R.string.unconfirmed, Toast.LENGTH_LONG).show();
				progress.setVisibility(ProgressBar.GONE);
				setTitle(getResources().getString(R.string.app_name));
				break;

			case HANDLER_REFRESH_TOO_SOON:
				// It's too soon to refresh the stats.
				Toast.makeText(getApplicationContext(), R.string.refresh_too_soon, Toast.LENGTH_LONG).show();
				progress.setVisibility(ProgressBar.GONE);
				setTitle(getResources().getString(R.string.app_name) + " - " + DateFormat.format("MM/dd/yy h:mmaa", last_refresh));
				break;

			case HANDLER_FATAL_ERROR:
				// Some fatal error
				Toast.makeText(getApplicationContext(), "Oops! " + ((Throwable)msg.obj).getMessage(), Toast.LENGTH_LONG).show();
				progress.setVisibility(ProgressBar.GONE);
				setTitle(getResources().getString(R.string.app_name));
				break;

			default:
				progress.setProgress(msg.what);
				break;
			}
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.event_list);

		progress = (ProgressBar) findViewById(R.id.progress);
		progress.setMax(100);

		db = new DatabaseHelper(this);
		cursor = db.getAllEvents();
		startManagingCursor(cursor);
		
		adapter = new EventAdapter(cursor, db);
		setListAdapter(adapter);
		registerForContextMenu(getListView());
	}

	@Override
	public void onResume() {
		super.onResume();
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		name = prefs.getString(PREF_NAME, "");
		platform = prefs.getString(PREF_PLATFORM, "");

		if (!prevName.equalsIgnoreCase(name)) {
			prevName = name;
			prevPlatform = platform;
			playerConfirmed = false;
			refreshData();
			return;
		}
	
		if (!prevPlatform.equalsIgnoreCase(platform)) {
			prevName = name;
			prevPlatform = platform;
			playerConfirmed = false;
			refreshData();
		}
	}

	private void refreshData() {
		if (!isOnline()) {
			Toast.makeText(this, R.string.internet_unavailable, Toast.LENGTH_LONG).show();
			return;
		}

		try {
			setTitle(getResources().getString(R.string.app_name) + getResources().getString(R.string.refreshing));
			ProgressBar progress = (ProgressBar) findViewById(R.id.progress);
			progress.setProgress(10);
			progress.setVisibility(ProgressBar.VISIBLE);
			Thread background = new Thread(new Runnable() {
				public void run() {
					try {
						if (!playerConfirmed) {
							playerConfirmed = api.isPlayerConfirmed(name, platform);
						}

						if (playerConfirmed) {
							Player player = api.getPlayer(name, platform);
							if (player == null) {	// too soon to refresh.
								handler.sendMessage(handler.obtainMessage(HANDLER_REFRESH_TOO_SOON));
								return;
							}
							db.refreshPlayer(player, handler);
							handler.sendMessage(handler.obtainMessage(HANDLER_REFRESH_FINISHED));
						} else {
							handler.sendMessage(handler.obtainMessage(HANDLER_UNCONFIRMED_PLAYER));
						}
					} catch (Throwable t) {
						handler.sendMessage(handler.obtainMessage(HANDLER_FATAL_ERROR, t));
					} 
				}
			});
			background.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		db.close();
		httpClient.getConnectionManager().shutdown();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		new MenuInflater(this).inflate(R.menu.list_menu, menu);
		return (super.onCreateOptionsMenu(menu));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.preferences:
			startActivity(new Intent(this, EditPreferences.class));
			return true;

		case R.id.help:
			startActivity(new Intent(this, Help.class));
			return true;

		case R.id.refresh:
			refreshData();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public boolean isOnline() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		if (cm == null) {
			return false;
		}

		if (cm.getActiveNetworkInfo() == null) {
			return false;
		}

		return cm.getActiveNetworkInfo().isConnected();
	}

	class EventAdapter extends CursorAdapter {
		private final ImageDownloader imageDownloader;

		public EventAdapter(Cursor c, DatabaseHelper db) {
			super(EventList.this, c);
			imageDownloader = new ImageDownloader(db);
		}

		@Override
		public void bindView(View row, Context context, Cursor cursor) {
			EventWrapper wrapper = (EventWrapper) row.getTag();
			wrapper.populateForm(cursor, imageDownloader);
		}

		@Override
		public View newView(Context context, Cursor cursor, ViewGroup parent) {
			LayoutInflater inflater = getLayoutInflater();
			View row = inflater.inflate(R.layout.event_row, parent, false);
			EventWrapper wrapper = new EventWrapper(row);

			row.setTag(wrapper);

			return row;
		}
	}

	class EventWrapper {
		private static final String BFBCS_SITE = "http://files2.bfbcs.com/img/bfbcs";
		private TextView name = null;
		private TextView kills = null;
		private ImageView eventImage = null;

		EventWrapper(View row) {
			name = (TextView) row.findViewById(R.id.name);
			kills = (TextView) row.findViewById(R.id.kills);
			eventImage = (ImageView) row.findViewById(R.id.event_image);
		}

		void populateForm(Cursor c, ImageDownloader imageDownloader) {
			name.setText(c.getString(c.getColumnIndex(DatabaseHelper.EVENT)));
			kills.setText(c.getString(c.getColumnIndex(DatabaseHelper.COUNT)));
			String imageName = c.getString(c.getColumnIndex(DatabaseHelper.IMG_RESOURCE));
			imageDownloader.download( BFBCS_SITE + imageName, eventImage);
		}
	}
}