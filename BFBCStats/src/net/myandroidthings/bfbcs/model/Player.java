package net.myandroidthings.bfbcs.model;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Player {
	public static final String WEAPONS = "weapons";
	public static final String GADGETS = "gadgets";
	public static final String INSIGINIAS = "insiginias";

	private String name;
	private String platform;
	private JSONObject json;
	ArrayList<Event> events = new ArrayList<Event>();

	public Player(String name, String platform, JSONObject json) {
		this.name = name;
		this.platform = platform;
		this.json = json;
	}

	public JSONObject getWeapons() throws JSONException {
		return json.getJSONObject(WEAPONS);
	}

	public JSONObject getGadgets() throws JSONException {
		return json.getJSONObject(GADGETS);
	}

	public JSONArray getInsiginias() throws JSONException {
		return json.getJSONArray(INSIGINIAS);
	}

	public String getName() {
		return name;
	}

	public String getPlatform() {
		return platform;
	}

	public JSONObject getJson() {
		return json;
	}
}
