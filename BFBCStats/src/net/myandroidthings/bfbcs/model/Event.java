package net.myandroidthings.bfbcs.model;

public class Event {
	public int killsLeft = 0;
	public String eventName = "";

	public Event(int killsLeft, String eventName) {
		this.killsLeft = killsLeft;
		this.eventName = eventName;
	}

	public String toString() {
		return eventName + " : " + killsLeft;
	}
}
